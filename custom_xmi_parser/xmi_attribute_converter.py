mapping_data = {
    'String': 'string',
    'Integer': 'number',
    'Boolean': 'boolean',
    'Image': 'string',
	'EString' : 'string'
}


def parse_uml_attribute(xmi_attribute_pathmap):
    attribute_data = xmi_attribute_pathmap.split('#')
    if len(attribute_data) > 1:
        print('UML attribute parse : ', end=' ')
        print(attribute_data[1])
        return attribute_data[1]
    else:
        return xmi_attribute_pathmap





def convert_uml_attribute_to_typescript(uml_attribute):
    print('Convert to TypeScript attribute parse : ', end=' ')
    print(mapping_data[uml_attribute])
    return mapping_data[uml_attribute]
