import logging
import sys
from main.utils import file_input_parser

from yattag import Doc

from custom_xmi_parser.xmiparser_2 import parse as uml_parse
from ifml_parser.ifmlxmiparser import parse as ifml_parse
from main.core.angular.interpreter.base import IFMLtoAngularInterpreter
from main.core.angular.project_writer import AngularProject
from main.core.angular.interpreter.json_to_model import convert_json as create_models_from_json
from main.utils.ast.framework.angular.base import AngularMainModule
from main.utils.ast.framework.angular.routers import AngularDefaultRouterDefinition
from main.utils.project_generator import create_structure
from main.utils.ast.framework.angular.routers import RootRoutingNode, RouteToModule
from main.utils.ast.framework.angular.components import AngularComponentTypescriptClass, AngularComponentHTML, AngularComponent, AngularRootComponentHTML
from main.utils.ast.framework.angular.google_sign_in import LoginHTML, LoginTypescriptClass
from main.utils.ast.framework.angular.not_login_component import NotLoginHTML, NotLoginTypescriptClass
from main.utils.ast.framework.angular.no_permission_component import NoPermissionHTML, NoPermissionTypescriptClass
from main.utils.ast.framework.angular.static_page_component import *
logger_angular = logging.getLogger("main.core.angular.main")


def generate_project(**kwargs):
    # Define default value for kwargs
    __define_default_values(kwargs)

    target_project_directory = sys.path[0] if kwargs["target_directory"] == '' else kwargs["target_directory"]
    basic_template = AngularProject(
        app_name=kwargs["product_name"],
        product_address=kwargs["product_address"],
        instance_port=kwargs["instance_port"],
        role = kwargs["role"])

    # Adding app.component.ts
    root_component_name = 'app'
    root_class_name = 'App'
    root_class_selector_name = 'app-root'

    # Defining Angular Main Module
    basic_app_module = AngularMainModule(app_name=kwargs["product_name"], google_client_id=kwargs["google_oauth2_client_id"])

    # Adding basic Routing Module
    basic_routing = AngularDefaultRouterDefinition()
    root_rooting = RootRoutingNode("")
    basic_template.add_default_app_component(__create_root_component().render())
    root_html_node = AngularRootComponentHTML()
    html_container = []
    list_service = []
    # Set up Angular Project
    __set_up_angular_project(kwargs, basic_template, root_rooting, basic_app_module,
                             basic_routing, list_service, html_container)
    print('Printing Html container...')
    print(html_container)
    root_html_node.body = html_container
    # fungsi show_login_button_in_root() di IFML interpreter base.py
    # Set up login button
    __set_up_login_button(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node)
    __set_up_not_login_template_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node)
    __set_up_no_permission_template_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node)
    __set_up_static_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node)
    basic_routing.add_routing_hierarchy(root_rooting)

    # Adding service worker config
    basic_template.write_service_worker_config(list_service)
    
    basic_template.add_app_html_template(root_html_node.render())

    if kwargs["enable_login"]:
        basic_app_module.enable_authentication_service()
        basic_template.enable_authentication_service()
        basic_routing.enable_authentication_service()

    
    # Adding App Module
    basic_template.add_app_module_file(basic_app_module.render())

    basic_template.add_app_module_routing(basic_routing.render())
    create_structure(basic_template.return_project_structure(), target_project_directory)
    logger_angular.info('Angular PWA Project successfully generated at ' + target_project_directory)


def __set_up_angular_project(kwargs, basic_template, root_rooting, basic_app_module, basic_routing,
                             list_service, html_container):
    for feature in kwargs["selected_feature"]:

        # If json are in the input parameter
        # Json name => interpreting_result.get_project_name()+'json'
        interpreting_result = __create_angular_interpreter(kwargs, feature, json_name=basic_template.get_app_name()+'.json')
        __adapt_models_from_json(interpreting_result, basic_template.get_app_name()+'.json')

        # Angular Typescript Component for root component
        # basic_template.add_default_app_component(interpreting_result.root_typescript_class.render())
        html_container +=  interpreting_result.root_html.body
        root_rooting.angular_children_routes.update(interpreting_result.angular_routing.angular_children_routes)

        # Define service worker
        __define_service_worker(list_service, interpreting_result)

        # Define model in the Angular Project
        __define_models(basic_template, interpreting_result)

        # Define AngularProject component
        __define_component(interpreting_result, basic_app_module, basic_routing, basic_template)

        # Define AngularProject Sevices
        __define_services(interpreting_result, basic_template)


def __create_root_component():
    root_component = AngularComponentTypescriptClass()
    root_component.component_name = 'app'
    root_component.class_name = 'App'
    root_component.selector_name = 'app-root'
    return root_component


def __define_default_values(kwargs):
    kwargs.setdefault('domain_name', 'localhost')
    kwargs.setdefault('target_directory', '')
    kwargs.setdefault('enable_login', True)
    kwargs.setdefault('port', 8089)


def __create_angular_interpreter(kwargs, feature, json_name=None):
    uml_structure, uml_symbol_table = uml_parse(kwargs["selected_feature"][feature][1])
    ifml_structure, ifml_symbol_table = ifml_parse(kwargs["selected_feature"][feature][0], uml_symbol_table)
    return IFMLtoAngularInterpreter(ifml_structure, ifml_symbol_table, uml_structure, uml_symbol_table,
                                    enable_authentication_guard=kwargs["enable_login"], json_model=json_name)


def __define_service_worker(list_service, interpreting_result):
    for service in interpreting_result.list_service_worker_config:
        list_service.append(service)


def __define_models(basic_template, interpreting_result):
    for _, model_node in interpreting_result.models.items():
        basic_template.add_model_inside_models_folder(model_node.render())


def __define_component(interpreting_result, basic_app_module, basic_routing, basic_template):
    # Adding each component of interpreting into the AngularProject
    for _, component_node in interpreting_result.components.items():
        # Insert the component into main module
        basic_app_module.add_component_to_module(component_node)

        # Importing all component to the routing node
        basic_routing.register_component_with_router(component_node)

        # Insert the component definition into src folder
        basic_template.add_new_component_using_basic_component_folder(component_node.build())


def __define_services(interpreting_result, basic_template):
    # Adding each service into the AngularProject
    for _, service_node in interpreting_result.services.items():
        # Insert the service into the services folder
        basic_template.add_service_inside_services_folder(service_node.render())

def __set_up_not_login_template_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node):
    html, typescript_class = NotLoginHTML(), NotLoginTypescriptClass()
    angular_component_node = AngularComponent(typescript_class, html)
    routing_node = RouteToModule(typescript_class)
    root_rooting.add_children_routing(routing_node)
    
    basic_app_module.add_component_to_module(angular_component_node)
    
    # Importing all component to the routing node
    basic_routing.register_component_with_router(angular_component_node)

    # Insert the component definition into src folder
    basic_template.add_new_component_using_basic_component_folder(angular_component_node.build())

def __set_up_no_permission_template_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node):
    html, typescript_class = NoPermissionHTML(), NoPermissionTypescriptClass()
    angular_component_node = AngularComponent(typescript_class, html)
    routing_node = RouteToModule(typescript_class)
    root_rooting.add_children_routing(routing_node)
    
    basic_app_module.add_component_to_module(angular_component_node)
    
    # Importing all component to the routing node
    basic_routing.register_component_with_router(angular_component_node)

    # Insert the component definition into src folder
    basic_template.add_new_component_using_basic_component_folder(angular_component_node.build())

def __set_up_static_page(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node):
    static_homepage_html, static_homepage_typescript_class = HomepageHTML(), HomepageTypescriptClass()
    static_about_html, static_about_typescript_class = AboutHTML(), AboutTypescriptClass()
    static_contact_html, static_contact_typescript_class = ContactHTML(), ContactTypescriptClass()

    static_homepage_angular_component_node = AngularComponent(static_homepage_typescript_class, static_homepage_html)
    static_about_angular_component_node = AngularComponent(static_about_typescript_class, static_about_html)
    static_contact_angular_component_node = AngularComponent(static_contact_typescript_class, static_contact_html)
    
    static_homepage_routing_node = RouteToModule(static_homepage_typescript_class)
    static_about_routing_node = RouteToModule(static_about_typescript_class)
    static_contact_routing_node = RouteToModule(static_contact_typescript_class)

    root_rooting.add_children_routing(static_homepage_routing_node)
    root_rooting.add_children_routing(static_about_routing_node)
    root_rooting.add_children_routing(static_contact_routing_node)
    
    basic_app_module.add_component_to_module(static_homepage_angular_component_node)
    basic_app_module.add_component_to_module(static_about_angular_component_node)
    basic_app_module.add_component_to_module(static_contact_angular_component_node)
    
    # Importing all component to the routing node
    basic_routing.register_component_with_router(static_homepage_angular_component_node)
    basic_routing.register_component_with_router(static_about_angular_component_node)
    basic_routing.register_component_with_router(static_contact_angular_component_node)

    # Insert the component definition into src folder
    basic_template.add_new_component_using_basic_component_folder(static_homepage_angular_component_node.build())
    basic_template.add_new_component_using_basic_component_folder(static_about_angular_component_node.build())
    basic_template.add_new_component_using_basic_component_folder(static_contact_angular_component_node.build())

def __set_up_login_button(root_rooting, basic_app_module, basic_routing, basic_template, root_html_node):
    if len(root_rooting.angular_children_routes) > 0:

        # TODO Implement, improve this logic
        # Create dummy AngularComponent
        html, typescript_class = LoginHTML(), LoginTypescriptClass()
        angular_component_node = AngularComponent(typescript_class, html)
        routing_node = RouteToModule(typescript_class)
        root_rooting.add_children_routing(routing_node)
        # component.components['login'] = angular_component_node
        basic_app_module.add_component_to_module(angular_component_node)

        # Importing all component to the routing node
        basic_routing.register_component_with_router(angular_component_node)

        # Insert the component definition into src folder
        basic_template.add_new_component_using_basic_component_folder(angular_component_node.build())
    else:
        doc_google_sign_in_button, tag_google_sign_in_button, text_google_sign_in_button = Doc().tagtext()
        with tag_google_sign_in_button('login'):
            text_google_sign_in_button('Google Sign In')
        root_html_node.append_html_into_body(doc_google_sign_in_button.getvalue())

def router_outlet_html(name):
    doc_outlet, tag_outlet, text_outlet = Doc().tagtext()
    with tag_outlet('div', id=name, klass='div-content-router'):
        with tag_outlet('div', id='main'):
            with tag_outlet('router-outlet'):
                text_outlet('')
    return doc_outlet.getvalue()

def create_router_outlet(angular_routing, html_call, name):
    if len(angular_routing.angular_children_routes) > 0:
        html_call.append_html_into_body(router_outlet_html(name))
        angular_routing.enable_children_routing()



def __adapt_models_from_json(interpreting_result, json_file_name):
    models_from_json = {}
    if file_input_parser.is_file_exists(json_file_name):
        print(json_file_name + ' found... Design adapted')
        for models_data in create_models_from_json(json_file_name):
            models_from_json[models_data.get_class_name()] = models_data
        interpreting_result.models = models_from_json
    # If file not found, use original design
    else:
        print('Use old design')
