from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer

class NoPermissionTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'error-page-no-permission'
        self.class_name = 'ErrorPageNoPermission'

    def render(self):
        return base_file_writer('src/app/error-page-no-permission/error-page-no-permission.component.ts.template')

    def get_class_name(self):
        return self.class_name

class NoPermissionHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/error-page-no-permission/error-page-no-permission.component.html.template')