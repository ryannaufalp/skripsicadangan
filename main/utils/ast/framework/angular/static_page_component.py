from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer

class HomepageTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'homepage'
        self.class_name = 'Homepage'

    def render(self):
        return base_file_writer('src/app/static-page/homepage/homepage.component.ts.template')

    def get_class_name(self):
        return self.class_name

class HomepageHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/static-page/homepage/homepage.component.html.template')

class AboutTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'about'
        self.class_name = 'About'

    def render(self):
        return base_file_writer('src/app/static-page/about/about.component.ts.template')

    def get_class_name(self):
        return self.class_name

class AboutHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/static-page/about/about.component.html.template')

class ContactTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'contact'
        self.class_name = 'Contact'

    def render(self):
        return base_file_writer('src/app/static-page/contact/contact.component.ts.template')

    def get_class_name(self):
        return self.class_name

class ContactHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/static-page/contact/contact.component.html.template')