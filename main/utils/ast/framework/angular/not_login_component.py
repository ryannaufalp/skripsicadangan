from main.utils.ast.base import Node
from main.utils.jinja.angular import base_file_writer

class NotLoginTypescriptClass(Node):

    def __init__(self):
        super().__init__()
        self.selector_name = 'error-page-not-login'
        self.class_name = 'ErrorPageNotLogin'

    def render(self):
        return base_file_writer('src/app/error-page-not-login/error-page-not-login.component.ts.template')

    def get_class_name(self):
        return self.class_name

class NotLoginHTML(Node):

    def __init__(self):
        super().__init__()

    def render(self):
        return base_file_writer('src/app/error-page-not-login/error-page-not-login.component.html.template')